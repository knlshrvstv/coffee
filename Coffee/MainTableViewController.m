//
//  ViewController.m
//  Coffee
//
//  Created by Kunal Shrivastava on 1/31/15.
//  Copyright (c) 2015 KunalShrivastava. All rights reserved.
//

#import "MainTableViewController.h"
#import "MainTableViewCell.h"
#import <AFNetworking/AFNetworking.h>

static NSString *CellIdentifier = @"CellIdentifier";
static NSString * const BaseURLString = @"https://coffeeapi.percolate.com/api/coffee/?api_key=WuVbkuUsCXHPx3hsQzus4SE";

@interface MainTableViewController ()

@property(strong,nonatomic) NSMutableArray *titleArray;
@property(strong,nonatomic) NSMutableArray *bodyArray;
@property(strong,nonatomic) NSMutableArray *coffees;

@end

@implementation MainTableViewController

BOOL fromDetails=NO;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
        self.title = @"Table View Controller";
    }
    return self;
}


- (void)viewDidLoad
{
    
    [super viewDidLoad];
    
    [self.tableView registerClass:[MainTableViewCell class] forCellReuseIdentifier:CellIdentifier];
    
    // 1
    NSString *string = BaseURLString;
    NSURL *url = [NSURL URLWithString:string];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    
    // 2
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    operation.responseSerializer = [AFJSONResponseSerializer serializer];
    
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        // 3
        self.coffees = (NSMutableArray *)responseObject;
        self.title = @"JSON Retrieved";
        [self.tableView reloadData];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        // 4
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error Retrieving Weather"
                                                            message:[error localizedDescription]
                                                           delegate:nil
                                                  cancelButtonTitle:@"Ok"
                                                  otherButtonTitles:nil];
        [alertView show];
    }];
    
    // 5
    [operation start];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)contentSizeCategoryChanged:(NSNotification *)notification
{
    [self.tableView reloadData];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    if(!_coffees)
    {
        return 0;
    }
    else
    {
        return _coffees.count;
    }
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    MainTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    //int dataIndex = (int) indexPath.row % [self.bodyArray count];
    
    NSDictionary *coffee=[[NSDictionary alloc] initWithDictionary:[_coffees objectAtIndex:indexPath.row]];
    
    cell.titleLabel.text = [coffee objectForKey: @"name"];
    cell.bodyLabel.text=[coffee objectForKey: @"desc"];
    
    //dispatch_async(dispatch_get_global_queue(0,0), ^{
        UIImage *gotImage=[UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:[coffee objectForKey: @"image_url"]]]];
                         
                         //dispatch_async(dispatch_get_main_queue(), ^{
            //PUT THE img INTO THE UIImageView (imgView for example)
            cell.coffeeImage.image=gotImage;
        //});
    //});
    
    

    // Make sure the constraints have been added to this cell, since it may have just been created from scratch
    [cell setNeedsUpdateConstraints];
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    MainTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    NSDictionary *coffee=[[NSDictionary alloc] initWithDictionary:[_coffees objectAtIndex:indexPath.row]];
    cell.titleLabel.text = [coffee objectForKey: @"name"];
    cell.bodyLabel.text=[coffee objectForKey: @"desc"];
    //dispatch_async(dispatch_get_global_queue(0,0), ^{
        UIImage *gotImage=[UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:[coffee objectForKey: @"image_url"]]]];
        
        //dispatch_async(dispatch_get_main_queue(), ^{
            //PUT THE img INTO THE UIImageView (imgView for example)
            cell.coffeeImage.image=gotImage;
        //});
    //});
    
    cell.bodyLabel.preferredMaxLayoutWidth = tableView.bounds.size.width - (20.0 * 2.0f);
    
    [cell setNeedsUpdateConstraints];
    [cell updateConstraintsIfNeeded];
    [cell.contentView setNeedsLayout];
    [cell.contentView layoutIfNeeded];
    
    CGFloat height = [cell.contentView systemLayoutSizeFittingSize:UILayoutFittingCompressedSize].height;
    
    return height;
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 200.0f;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    //NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
    DetailViewController *detailViewController = [[DetailViewController alloc] init];
    detailViewController.delegate=self;
    
    if([[_coffees objectAtIndex:indexPath.row] objectForKey: @"name"]==[NSNull null])
    {
        detailViewController.titleLabelText = @"-";
    }
    else
    {
        detailViewController.titleLabelText = [[_coffees objectAtIndex:indexPath.row] objectForKey: @"name"];
    }
    
    if([[_coffees objectAtIndex:indexPath.row] objectForKey: @"desc"]==[NSNull null])
    {
        detailViewController.bodyLabelText = @"-";
    }
    else
    {
        detailViewController.bodyLabelText = [[_coffees objectAtIndex:indexPath.row] objectForKey: @"desc"];
    }
    
    if([[_coffees objectAtIndex:indexPath.row] objectForKey: @"image_url"]==[NSNull null])
    {
        detailViewController.coffeeImagePic = [UIImage imageNamed:@"no_image.jpg"];;
    }
    else
    {
        detailViewController.coffeeImagePic = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:[[_coffees objectAtIndex:indexPath.row] objectForKey: @"image_url"]]]];
    }
    

    
    [self.navigationController pushViewController:detailViewController animated:YES];
    
    //Value Selected by user
    //NSString *selectedValue = [displayValues objectAtIndex:indexPath.row];
    /*
    //Initialize new viewController
    NewViewController *viewController = [[NewViewController alloc] initWithNibName:@"NewViewController" bundle:nil];
    //Pass selected value to a property declared in NewViewController
    viewController.valueToPrint = selectedValue;
    //Push new view to navigationController stack
    [self.navigationController pushViewController:viewController animated:YES];
     */
}

#pragma mark - DetailsViewControllerDelegate

- (void)fromDetailViewController
{
    fromDetails=YES;
}


@end
