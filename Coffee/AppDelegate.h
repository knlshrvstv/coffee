//
//  AppDelegate.h
//  Coffee
//
//  Created by Kunal Shrivastava on 1/31/15.
//  Copyright (c) 2015 KunalShrivastava. All rights reserved.
//

#import <UIKit/UIKit.h>

@class MainTableViewController;

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (strong,nonatomic) MainTableViewController *mainTableViewController;


@end

