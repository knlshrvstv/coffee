//
//  DetailViewController.m
//  Coffee
//
//  Created by Kunal Shrivastava on 2/1/15.
//  Copyright (c) 2015 KunalShrivastava. All rights reserved.
//

#import "DetailViewController.h"

@interface DetailViewController ()

@property (nonatomic, assign) BOOL didSetupConstraints;

@end

@implementation DetailViewController



- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(40, 20, 300, 100)];
    [self.titleLabel setTranslatesAutoresizingMaskIntoConstraints:NO];
    [self.titleLabel setLineBreakMode:NSLineBreakByTruncatingTail];
    [self.titleLabel setNumberOfLines:1];
    [self.titleLabel setTextAlignment:NSTextAlignmentLeft];
    [self.titleLabel setTextColor:[UIColor blackColor]];
    [self.titleLabel setBackgroundColor:[UIColor clearColor]];
    [self.view addSubview:self.titleLabel];
    
    // Add this label to the button
    self.bodyLabel = [[UILabel alloc] initWithFrame:CGRectMake(40, 50, 300, 100)];
    [self.bodyLabel setTranslatesAutoresizingMaskIntoConstraints:NO];
    [self.bodyLabel setContentCompressionResistancePriority:UILayoutPriorityRequired forAxis:UILayoutConstraintAxisVertical];
    [self.bodyLabel setLineBreakMode:NSLineBreakByTruncatingTail];
    [self.bodyLabel setNumberOfLines:5];
    [self.bodyLabel setTextAlignment:NSTextAlignmentLeft];
    [self.bodyLabel setTextColor:[UIColor darkGrayColor]];
    [self.bodyLabel setBackgroundColor:[UIColor clearColor]];
    [self.view addSubview:self.bodyLabel];
    
    // Add this image to the button
    self.coffeeImage = [[UIImageView alloc] init];
    [self.coffeeImage setTranslatesAutoresizingMaskIntoConstraints:NO];
    [self.coffeeImage setContentCompressionResistancePriority:UILayoutPriorityRequired forAxis:UILayoutConstraintAxisVertical];
    self.coffeeImage.contentMode = UIViewContentModeScaleAspectFill;
    self.coffeeImage.clipsToBounds = YES;
    [self.view addSubview:self.coffeeImage];
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"|[_titleLabel]|"
                                                                      options:kNilOptions
                                                                      metrics:nil
                                                                        views:@{ @"_titleLabel" : self.titleLabel }]];
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"|[_bodyLabel]|"
                                                                      options:kNilOptions
                                                                      metrics:nil
                                                                        views:@{ @"_bodyLabel" : self.bodyLabel }]];
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"|[_coffeeImage]|"
                                                                      options:kNilOptions
                                                                      metrics:nil
                                                                        views:@{ @"_coffeeImage" : self.coffeeImage }]];
    
    NSString *format;
    NSArray *constraintsArray;
    
    NSDictionary *viewsDictionary =
    @{
      @"_titleLabel" : self.titleLabel,
      @"_bodyLabel" : self.bodyLabel,
      @"_coffeeImage" : self.coffeeImage
      };

    
    format = @"V:|[_titleLabel][_bodyLabel][_coffeeImage(<=200)]|";
    constraintsArray = [NSLayoutConstraint constraintsWithVisualFormat:format options:0 metrics:nil views:viewsDictionary];
    [self.view addConstraints:constraintsArray];
    
    UIBarButtonItem *shareButton = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemAction target:self action:@selector(fbShare)];
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(doneAction)];
    self.navigationController.navigationBarHidden=NO;
    //self.navigationItem.leftBarButtonItem=backButton;
    self.navigationItem.rightBarButtonItem=shareButton;
    self.titleLabel.text=self.titleLabelText;
    self.bodyLabel.text=self.bodyLabelText;
    self.coffeeImage.image=self.coffeeImagePic;
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
