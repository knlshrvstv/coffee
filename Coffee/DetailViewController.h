//
//  DetailViewController.h
//  Coffee
//
//  Created by Kunal Shrivastava on 2/1/15.
//  Copyright (c) 2015 KunalShrivastava. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol DetailViewControllerDelegate <NSObject>
- (void)fromDetailViewController;
@end

@interface DetailViewController : UIViewController

@property (nonatomic, weak) id <DetailViewControllerDelegate> delegate;

@property (strong, nonatomic) UILabel *titleLabel;
@property (strong, nonatomic) UILabel *bodyLabel;
@property (strong, nonatomic) UIImageView *coffeeImage;

@property (strong, nonatomic) NSString *titleLabelText;
@property (strong, nonatomic) NSString *bodyLabelText;
@property (strong, nonatomic) UIImage *coffeeImagePic;

@end
