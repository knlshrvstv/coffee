//
//  MainTableViewCell.h
//  Coffee
//
//  Created by Kunal Shrivastava on 1/31/15.
//  Copyright (c) 2015 KunalShrivastava. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MainTableViewCell : UITableViewCell

@property (strong, nonatomic) UILabel *titleLabel;
@property (strong, nonatomic) UILabel *bodyLabel;
@property (strong, nonatomic) UIImageView *coffeeImage;

@end
