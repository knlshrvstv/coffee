//
//  MainTableViewCell.m
//  Coffee
//
//  Created by Kunal Shrivastava on 1/31/15.
//  Copyright (c) 2015 KunalShrivastava. All rights reserved.
//

#import "MainTableViewCell.h"

@interface MainTableViewCell ()

@property (nonatomic, assign) BOOL didSetupConstraints;

@end

@implementation MainTableViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        
        self.titleLabel = [[UILabel alloc] init];
        [self.titleLabel setTranslatesAutoresizingMaskIntoConstraints:NO];
        [self.titleLabel setLineBreakMode:NSLineBreakByTruncatingTail];
        [self.titleLabel setNumberOfLines:1];
        [self.titleLabel setTextAlignment:NSTextAlignmentLeft];
        [self.titleLabel setTextColor:[UIColor blackColor]];
        [self.titleLabel setBackgroundColor:[UIColor clearColor]];
        [self.contentView addSubview:self.titleLabel];
        
        // Add this label to the button
        self.bodyLabel = [[UILabel alloc] init];
        [self.bodyLabel setTranslatesAutoresizingMaskIntoConstraints:NO];
        [self.bodyLabel setContentCompressionResistancePriority:UILayoutPriorityRequired forAxis:UILayoutConstraintAxisVertical];
        [self.bodyLabel setLineBreakMode:NSLineBreakByTruncatingTail];
        [self.bodyLabel setNumberOfLines:0];
        [self.bodyLabel setTextAlignment:NSTextAlignmentLeft];
        [self.bodyLabel setTextColor:[UIColor darkGrayColor]];
        [self.bodyLabel setBackgroundColor:[UIColor clearColor]];
        [self.contentView addSubview:self.bodyLabel];
        
        // Add this image to the button
        self.coffeeImage = [[UIImageView alloc] init];
        [self.coffeeImage setTranslatesAutoresizingMaskIntoConstraints:NO];
        [self.coffeeImage setContentCompressionResistancePriority:UILayoutPriorityRequired forAxis:UILayoutConstraintAxisVertical];
        self.coffeeImage.contentMode = UIViewContentModeScaleAspectFill;
        self.coffeeImage.clipsToBounds = YES;
        [self.contentView addSubview:self.coffeeImage];
    }
    
    return self;
}

- (void)updateConstraints {
    [super updateConstraints];
    
    if (self.didSetupConstraints) return;
    
    // Get the views dictionary
    NSDictionary *viewsDictionary =
    @{
      @"titleLabel" : self.titleLabel,
      @"bodyLabel" : self.bodyLabel,
      @"coffeeImage" : self.coffeeImage
      };
    
    NSString *format;
    NSArray *constraintsArray;
    
    //Create the constraints using the visual language format
    format = @"V:|-10-[titleLabel]-10-[bodyLabel]-10-[coffeeImage(<=200)]-10-|";
    constraintsArray = [NSLayoutConstraint constraintsWithVisualFormat:format options:0 metrics:nil views:viewsDictionary];
    [self.contentView addConstraints:constraintsArray];
    
    format = @"|-10-[titleLabel]-10-|";
    constraintsArray = [NSLayoutConstraint constraintsWithVisualFormat:format options:0 metrics:nil views:viewsDictionary];
    [self.contentView addConstraints:constraintsArray];
    
    format = @"|-10-[bodyLabel]-10-|";
    constraintsArray = [NSLayoutConstraint constraintsWithVisualFormat:format options:0 metrics:nil views:viewsDictionary];
    [self.contentView addConstraints:constraintsArray];
    
    format = @"|-10-[coffeeImage]-10-|";
    constraintsArray = [NSLayoutConstraint constraintsWithVisualFormat:format options:0 metrics:nil views:viewsDictionary];
    [self.contentView addConstraints:constraintsArray];
    
    self.didSetupConstraints = YES;
}

@end
