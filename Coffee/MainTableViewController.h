//
//  ViewController.h
//  Coffee
//
//  Created by Kunal Shrivastava on 1/31/15.
//  Copyright (c) 2015 KunalShrivastava. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DetailViewController.h"

@interface MainTableViewController : UITableViewController <DetailViewControllerDelegate>


@end

